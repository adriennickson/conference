<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\Cout;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Cout controller.
 *
 * @Route("/admin/cout")
 */
class CoutController extends Controller
{
    /**
     * Lists all cout entities.
     *
     * @Route("/", name="admin_cout_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $couts = $em->getRepository('AppBundle:Cout')->findAll();

        return $this->render('@Admin/cout/index.html.twig', array(
            'couts' => $couts,
        ));
    }

    /**
     * Creates a new cout entity.
     *
     * @Route("/new", name="admin_cout_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $cout = new Cout();
        $form = $this->createForm('AppBundle\Form\CoutType', $cout);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cout);
            $em->flush();

            return $this->redirectToRoute('admin_cout_show', array('id' => $cout->getId()));
        }

        return $this->render('@Admin/cout/new.html.twig', array(
            'cout' => $cout,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a cout entity.
     *
     * @Route("/{id}", name="admin_cout_show")
     * @Method("GET")
     */
    public function showAction(Cout $cout)
    {
        $deleteForm = $this->createDeleteForm($cout);

        return $this->render('@Admin/cout/show.html.twig', array(
            'cout' => $cout,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing cout entity.
     *
     * @Route("/{id}/edit", name="admin_cout_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Cout $cout)
    {
        $deleteForm = $this->createDeleteForm($cout);
        $editForm = $this->createForm('AppBundle\Form\CoutType', $cout);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_cout_edit', array('id' => $cout->getId()));
        }

        return $this->render('@Admin/cout/edit.html.twig', array(
            'cout' => $cout,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a cout entity.
     *
     * @Route("/{id}", name="admin_cout_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Cout $cout)
    {
        $form = $this->createDeleteForm($cout);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($cout);
            $em->flush();
        }

        return $this->redirectToRoute('admin_cout_index');
    }

    /**
     * Creates a form to delete a cout entity.
     *
     * @param Cout $cout The cout entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cout $cout)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_cout_delete', array('id' => $cout->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
