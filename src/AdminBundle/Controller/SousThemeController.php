<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\SousTheme;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Soustheme controller.
 *
 * @Route("/back-office/soustheme")
 */
class SousThemeController extends Controller
{
    /**
     * Lists all sousTheme entities.
     *
     * @Route("/", name="soustheme_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sousThemes = $em->getRepository('AppBundle:SousTheme')->findAll();

        return $this->render('@Admin/soustheme/index.html.twig', array(
            'sousThemes' => $sousThemes,
        ));
    }

    /**
     * Creates a new sousTheme entity.
     *
     * @Route("/new", name="soustheme_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $sousTheme = new Soustheme();
        $form = $this->createForm('AppBundle\Form\SousThemeType', $sousTheme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($sousTheme);
            $em->flush();

            return $this->redirectToRoute('soustheme_show', array('id' => $sousTheme->getId()));
        }

        return $this->render('@Admin/soustheme/new.html.twig', array(
            'sousTheme' => $sousTheme,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a sousTheme entity.
     *
     * @Route("/{id}", name="soustheme_show")
     * @Method("GET")
     */
    public function showAction(SousTheme $sousTheme)
    {
        $deleteForm = $this->createDeleteForm($sousTheme);

        return $this->render('@Admin/soustheme/show.html.twig', array(
            'sousTheme' => $sousTheme,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing sousTheme entity.
     *
     * @Route("/{id}/edit", name="soustheme_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SousTheme $sousTheme)
    {
        $deleteForm = $this->createDeleteForm($sousTheme);
        $editForm = $this->createForm('AppBundle\Form\SousThemeType', $sousTheme);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('soustheme_edit', array('id' => $sousTheme->getId()));
        }

        return $this->render('@Admin/soustheme/edit.html.twig', array(
            'sousTheme' => $sousTheme,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a sousTheme entity.
     *
     * @Route("/{id}", name="soustheme_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SousTheme $sousTheme)
    {
        $form = $this->createDeleteForm($sousTheme);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sousTheme);
            $em->flush();
        }

        return $this->redirectToRoute('soustheme_index');
    }

    /**
     * Creates a form to delete a sousTheme entity.
     *
     * @param SousTheme $sousTheme The sousTheme entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SousTheme $sousTheme)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('soustheme_delete', array('id' => $sousTheme->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
