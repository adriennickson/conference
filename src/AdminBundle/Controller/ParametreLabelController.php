<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\ParametreLabel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Parametrelabel controller.
 *
 * @Route("parametrelabel")
 */
class ParametreLabelController extends Controller
{
    /**
     * Lists all parametreLabel entities.
     *
     * @Route("/", name="parametrelabel_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $parametreLabels = $em->getRepository('AppBundle:ParametreLabel')->findAll();

        return $this->render('@Admin/parametrelabel/index.html.twig', array(
            'parametreLabels' => $parametreLabels,
        ));
    }

    /**
     * Creates a new parametreLabel entity.
     *
     * @Route("/new", name="parametrelabel_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $parametreLabel = new Parametrelabel();
        $form = $this->createForm('AppBundle\Form\ParametreLabelType', $parametreLabel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($parametreLabel);
            $em->flush();

            return $this->redirectToRoute('parametrelabel_show', array('id' => $parametreLabel->getId()));
        }

        return $this->render('@Admin/parametrelabel/new.html.twig', array(
            'parametreLabel' => $parametreLabel,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a parametreLabel entity.
     *
     * @Route("/{id}", name="parametrelabel_show")
     * @Method("GET")
     */
    public function showAction(ParametreLabel $parametreLabel)
    {
        $deleteForm = $this->createDeleteForm($parametreLabel);

        return $this->render('@Admin/parametrelabel/show.html.twig', array(
            'parametreLabel' => $parametreLabel,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing parametreLabel entity.
     *
     * @Route("/{id}/edit", name="parametrelabel_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ParametreLabel $parametreLabel)
    {
        $deleteForm = $this->createDeleteForm($parametreLabel);
        $editForm = $this->createForm('AppBundle\Form\ParametreLabelType', $parametreLabel);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('parametrelabel_edit', array('id' => $parametreLabel->getId()));
        }

        return $this->render('@Admin/parametrelabel/edit.html.twig', array(
            'parametreLabel' => $parametreLabel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a parametreLabel entity.
     *
     * @Route("/{id}", name="parametrelabel_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ParametreLabel $parametreLabel)
    {
        $form = $this->createDeleteForm($parametreLabel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($parametreLabel);
            $em->flush();
        }

        return $this->redirectToRoute('parametrelabel_index');
    }

    /**
     * Creates a form to delete a parametreLabel entity.
     *
     * @param ParametreLabel $parametreLabel The parametreLabel entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ParametreLabel $parametreLabel)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('parametrelabel_delete', array('id' => $parametreLabel->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
