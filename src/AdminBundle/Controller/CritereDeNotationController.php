<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\CritereDeNotation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Criteredenotation controller.
 *
 * @Route("admin/criteredenotation")
 */
class CritereDeNotationController extends Controller
{
    /**
     * Lists all critereDeNotation entities.
     *
     * @Route("/", name="criteredenotation_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $critereDeNotations = $em->getRepository('AppBundle:CritereDeNotation')->findAll();

        return $this->render('@Admin/criteredenotation/index.html.twig', array(
            'critereDeNotations' => $critereDeNotations,
        ));
    }

    /**
     * Creates a new critereDeNotation entity.
     *
     * @Route("/new", name="criteredenotation_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $critereDeNotation = new Criteredenotation();
        $form = $this->createForm('AppBundle\Form\CritereDeNotationType', $critereDeNotation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($critereDeNotation);
            $em->flush();

            return $this->redirectToRoute('criteredenotation_show', array('id' => $critereDeNotation->getId()));
        }

        return $this->render('@Admin/criteredenotation/new.html.twig', array(
            'critereDeNotation' => $critereDeNotation,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a critereDeNotation entity.
     *
     * @Route("/{id}", name="criteredenotation_show")
     * @Method("GET")
     */
    public function showAction(CritereDeNotation $critereDeNotation)
    {
        $deleteForm = $this->createDeleteForm($critereDeNotation);

        return $this->render('@Admin/criteredenotation/show.html.twig', array(
            'critereDeNotation' => $critereDeNotation,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing critereDeNotation entity.
     *
     * @Route("/{id}/edit", name="criteredenotation_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CritereDeNotation $critereDeNotation)
    {
        $deleteForm = $this->createDeleteForm($critereDeNotation);
        $editForm = $this->createForm('AppBundle\Form\CritereDeNotationType', $critereDeNotation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('criteredenotation_edit', array('id' => $critereDeNotation->getId()));
        }

        return $this->render('@Admin/criteredenotation/edit.html.twig', array(
            'critereDeNotation' => $critereDeNotation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a critereDeNotation entity.
     *
     * @Route("/{id}", name="criteredenotation_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CritereDeNotation $critereDeNotation)
    {
        $form = $this->createDeleteForm($critereDeNotation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($critereDeNotation);
            $em->flush();
        }

        return $this->redirectToRoute('criteredenotation_index');
    }

    /**
     * Creates a form to delete a critereDeNotation entity.
     *
     * @param CritereDeNotation $critereDeNotation The critereDeNotation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CritereDeNotation $critereDeNotation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('criteredenotation_delete', array('id' => $critereDeNotation->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
