<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\MotClef;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Motclef controller.
 *
 * @Route("admin/motclef")
 */
class MotClefController extends Controller
{
    /**
     * Lists all motClef entities.
     *
     * @Route("/", name="admin_motclef_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $motClefs = $em->getRepository('AppBundle:MotClef')->findAll();

        return $this->render('@Admin/motclef/index.html.twig', array(
            'motClefs' => $motClefs,
        ));
    }

    /**
     * Creates a new motClef entity.
     *
     * @Route("/new", name="admin_motclef_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $motClef = new Motclef();
        $form = $this->createForm('AppBundle\Form\MotClefType', $motClef);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($motClef);
            $em->flush();

            return $this->redirectToRoute('admin_motclef_show', array('id' => $motClef->getId()));
        }

        return $this->render('@Admin/motclef/new.html.twig', array(
            'motClef' => $motClef,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a motClef entity.
     *
     * @Route("/{id}", name="admin_motclef_show")
     * @Method("GET")
     */
    public function showAction(MotClef $motClef)
    {
        $deleteForm = $this->createDeleteForm($motClef);

        return $this->render('@Admin/motclef/show.html.twig', array(
            'motClef' => $motClef,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing motClef entity.
     *
     * @Route("/{id}/edit", name="admin_motclef_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, MotClef $motClef)
    {
        $deleteForm = $this->createDeleteForm($motClef);
        $editForm = $this->createForm('AppBundle\Form\MotClefType', $motClef);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_motclef_edit', array('id' => $motClef->getId()));
        }

        return $this->render('@Admin/motclef/edit.html.twig', array(
            'motClef' => $motClef,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a motClef entity.
     *
     * @Route("/{id}", name="admin_motclef_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, MotClef $motClef)
    {
        $form = $this->createDeleteForm($motClef);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($motClef);
            $em->flush();
        }

        return $this->redirectToRoute('admin_motclef_index');
    }

    /**
     * Creates a form to delete a motClef entity.
     *
     * @param MotClef $motClef The motClef entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MotClef $motClef)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_motclef_delete', array('id' => $motClef->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
