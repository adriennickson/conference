<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class RelecteurController extends Controller
{
    /**
     * @Route("/admin/relecteur/index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $relecteurs = $em->getRepository('AppBundle:User')->findByRole("ROLE_RELECTEUR");
        return $this->render('AdminBundle:Relecteur:index.html.twig', array(
            "relecteurs" => $relecteurs
        ));
    }

    /**
     * @Route("/admin/relecteur/add")
     */
    public function addAction()
    {
        $em = $this->getDoctrine()->getManager();
        $relecteurs = $em->getRepository('AppBundle:User')->findByRoleNot("ROLE_RELECTEUR");
        return $this->render('AdminBundle:Relecteur:add.html.twig', array(
            "relecteurs" => $relecteurs
        ));
    }
    /**
     * @Route("/admin/relecteur/set/{id}")
     */
    public function setAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $newRelecteur =  $em->getRepository('AppBundle:User')->find($id);
        $newRelecteur->addRole("ROLE_RELECTEUR");
        $em->persist($newRelecteur);
        $em->flush();
        return $this->redirectToRoute('admin_relecteur_add');
    }
    /**
     * @Route("/admin/relecteur/unset/{id}")
     */
    public function unsetAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $newRelecteur =  $em->getRepository('AppBundle:User')->find($id);
        $newRelecteur->removeRole("ROLE_RELECTEUR");
        $em->persist($newRelecteur);
        $em->flush();
        return $this->redirectToRoute('admin_relecteur_index');
    }
}
