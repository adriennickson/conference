<?php

namespace AdminBundle\Controller;

use AppBundle\Entity\AbstractHasRelecteur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AbstractClassControllerController extends Controller
{
    /**
     * @Route("/admin/setRelecteurs/{id}", requirements={"id" = "\d+"})
     */
    public function setRelecteursAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $abstractClass = $em->getRepository('AppBundle:AbstractClass')->find($id);
        $users = $em->getRepository('AppBundle:User')->findByRole("ROLE_RELECTEUR");
        return $this->render('AdminBundle:AbstractClassController:set_relecteurs.html.twig', array(
            'utilisateurs' => $users,
            'abstractClass' => $abstractClass
        ));
    }

    /**
     * @Route("/admin/addRelecteurs/{idabstract}.{iduser}", requirements={"idabstract" = "\d+","iduser" = "\d+"})
     */
    public function addRelecteursAction($idabstract,$iduser)
    {
        $em = $this->getDoctrine()->getManager();

        $abstractClass = $em->getRepository('AppBundle:AbstractClass')->find($idabstract);
        $user = $em->getRepository('AppBundle:User')->find($iduser);
        $aar = new AbstractHasRelecteur();
        $aar->setAbstract($abstractClass);
        $aar->setRelecteur($user);
        if (!in_array("ROLE_RELECTEUR",$user->getRoles()))
            $user->addRole("ROLE_RELECTEUR");
        $em->persist($aar);
        $em->persist($abstractClass);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute("admin_abstractclasscontroller_setrelecteurs",array('id'=>$abstractClass->getId()));
    }

    /**
     * @Route("/admin/removeRelecteurs/{idabstract}.{iduser}", requirements={"idabstract" = "\d+","iduser" = "\d+"})
     */
    public function removeRelecteursAction($idabstract,$iduser)
    {
        $em = $this->getDoctrine()->getManager();
        $abstractClass = $em->getRepository('AppBundle:AbstractClass')->find($idabstract);
        $user = $em->getRepository('AppBundle:User')->find($iduser);
        $abstractHasRelecteur = $em->getRepository('AppBundle:AbstractHasRelecteur')->findOneBy(array(
            'abstract' => $abstractClass,
            'relecteur' => $user
        ));
        $user->removeAbstractHasRelecteur($abstractHasRelecteur);
/*        if($user->getAbstractHasRelecteurs()->count()==0)
            $user->removeRole("ROLE_RELECTEUR");*/
        $abstractClass->removeAbstractHasRelecteur($abstractHasRelecteur);
        $em->persist($abstractClass);
        $em->persist($user);
        $em->remove($abstractHasRelecteur);
        $em->flush();

        return $this->redirectToRoute("admin_abstractclasscontroller_setrelecteurs",array('id'=>$abstractClass->getId()));
    }

}
