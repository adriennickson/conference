<?php

namespace BackOfficeBundle\Controller;

use AppBundle\Entity\ListOfNote;
use AppBundle\Entity\NoteAbstract;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class RelectureController extends Controller
{
    /**
     * @Route("/index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $abstractClasses = $em->getRepository('AppBundle:AbstractClass')->findByRelecteur($this->getUser());

        return $this->render('BackOfficeBundle:Relecture:index.html.twig', array(
            'abstractClasses' => $abstractClasses,
        ));
    }

    /**
     * @Route("/noter/{abstractId}")
     */
    public function noterAction(Request $request, $abstractId)
    {
        $em = $this->getDoctrine()->getManager();

        $abstractClasse = $em->getRepository('AppBundle:AbstractClass')->find($abstractId);
        $abstractHasRelecteur = $em->getRepository('AppBundle:AbstractHasRelecteur')->findOneBy(
            array(
                'relecteur' => $this->getUser(),
                'abstract' => $abstractClasse
            )
        );
        $criteres = $em->getRepository('AppBundle:CritereDeNotation')->findAll();
        $liste = new ListOfNote();
        foreach ($criteres as $critere)
        {
            $note = $em->getRepository('AppBundle:NoteAbstract')->findOneBy(array(
                'abstractHasRelecteur' => $abstractHasRelecteur,
                'critereDeNotation' => $critere
            ));
            if ($note == null ){
                $note = new NoteAbstract();
                $note->setAbstractHasRelecteur($abstractHasRelecteur);
                $note->setCritereDeNotation($critere);
            }
            $liste->addNotes($note);
        }
        dump($liste);
        $form = $this->createForm('AppBundle\Form\ListOfNoteType', $liste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            foreach ($liste->getNotes() as $note)
            {
                $em->persist($note);
            }
            $abstractHasRelecteur->setObservation($liste->getObservation());
            $abstractHasRelecteur->setCommentaire($liste->getComentaire());
            $em->flush();

            return $this->redirectToRoute('backoffice_relecture_index');
        }

        return $this->render('BackOfficeBundle:Relecture:noter.html.twig', array(
            'abstractClass' => $abstractClasse,
            'form' => $form->createView()
        ));
    }

}
