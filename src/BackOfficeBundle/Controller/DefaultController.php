<?php

namespace BackOfficeBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $nbreUtilisateur=$em->getRepository('AppBundle:User')->findAll();
        $nbreAbstract=$em->getRepository('AppBundle:AbstractClass')->findAll();
        $nbreU=count($nbreUtilisateur);
        $nbreA=count($nbreAbstract);

        return $this->render('BackOfficeBundle:Default:index.html.twig',array(
            'nbreUtilisateur'=>$nbreU,
            'nbreAbstract'=>$nbreA
        ));
    }
}
