<?php

namespace AppBundle\Twig\Extension;

use Symfony\Bridge\Doctrine\RegistryInterface;

class ConferenceExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'conference';
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('abstractHasRelecteur', array($this, 'getAbstractHasRelecteur')),
            new \Twig_SimpleFunction('numberOfUsers', array($this, 'getNumberOfUsers')),
            new \Twig_SimpleFunction('numberOfAbstracts', array($this, 'getNumberOfAbstracts')),
            new \Twig_SimpleFunction('sousThemes', array($this, 'getSousThemes')),
            new \Twig_SimpleFunction('param', array($this, 'getParam')),
            new \Twig_SimpleFunction('speakers', array($this, 'getSpeakers')),
        );
    }

    protected $doctrine;

    public function __construct(RegistryInterface $doctrine)
    {
        $this->doctrine=$doctrine;
    }

    public function getNumberOfAbstracts()
    {
        $em = $this->doctrine->getManager();
        $myRepo = $em->getRepository('AppBundle:AbstractClass');
        $ahs = $myRepo->findAll();
        return count($ahs);
    }

    public function getNumberOfUsers()
    {
        $em = $this->doctrine->getManager();
        $myRepo = $em->getRepository('AppBundle:User');
        $ahs = $myRepo->findAll();
        return count($ahs);
    }

    public function getAbstractHasRelecteur($abstractClass, $relecteur)
    {
        $em = $this->doctrine->getManager();
        $myRepo = $em->getRepository('CoreBundle:Message');
        $ahs = $myRepo->find(array(
            'abstract' => $abstractClass,
            'relecteur' => $relecteur
        ));
        return $ahs;
    }

    public function getSousThemes()
    {
        $em = $this->doctrine->getManager();
        $myRepo = $em->getRepository('AppBundle:SousTheme');
        $st = $myRepo->findAll();
        return $st;
    }

    public function getParam($clef)
    {
        $em = $this->doctrine->getManager();
        $myRepo = $em->getRepository('AppBundle:ParametreLabel');
        $st = $myRepo->findOneBy(array('ref' => $clef));
        if($st != null)
            return $st->getParametre()->getValeur();
        return null;
    }

    public function getSpeakers()
    {
        $em = $this->doctrine->getManager();
        $myRepo = $em->getRepository('AppBundle:Speaker');
        $st = $myRepo->findAll();
        return $st;
    }
}
