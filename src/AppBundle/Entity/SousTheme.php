<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SousTheme
 *
 * @ORM\Table(name="sous_theme")
 * @ORM\Entity
 */
class SousTheme
{
    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=45, nullable=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=45, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\AbstractClass", mappedBy="soustheme")
     */
    private $abstract;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Session", mappedBy="soustheme")
     */
    private $session;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->abstract = new \Doctrine\Common\Collections\ArrayCollection();
        $this->session = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return SousTheme
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SousTheme
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add abstract
     *
     * @param \AppBundle\Entity\AbstractClass $abstract
     *
     * @return SousTheme
     */
    public function addAbstract(\AppBundle\Entity\AbstractClass $abstract)
    {
        $this->abstract[] = $abstract;

        return $this;
    }

    /**
     * Remove abstract
     *
     * @param \AppBundle\Entity\AbstractClass $abstract
     */
    public function removeAbstract(\AppBundle\Entity\AbstractClass $abstract)
    {
        $this->abstract->removeElement($abstract);
    }

    /**
     * Get abstract
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * Add session
     *
     * @param \AppBundle\Entity\Session $session
     *
     * @return SousTheme
     */
    public function addSession(\AppBundle\Entity\Session $session)
    {
        $this->session[] = $session;

        return $this;
    }

    /**
     * Remove session
     *
     * @param \AppBundle\Entity\Session $session
     */
    public function removeSession(\AppBundle\Entity\Session $session)
    {
        $this->session->removeElement($session);
    }

    /**
     * Get session
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSession()
    {
        return $this->session;
    }
}
