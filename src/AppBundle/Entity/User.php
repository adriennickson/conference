<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $salutation;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $lastName;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $jobTitle;

    /**
     * @var Institution
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Inscription")
     */
    private $institution;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $addressLine;

    /**
     * @var Collection
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Telephone",
     *     mappedBy="user",
     *     orphanRemoval=true,
     *     cascade={"persist","remove"}
     * )
     */
    private $telephones;

    /**
     * @var Localisation
     * @ORM\OneToOne(
     *     targetEntity="AppBundle\Entity\Localisation",
     * )
     * @ORM\JoinColumn(nullable=true)
     */
    private $localisation;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $website;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $domaineEtude;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $niveauAcademique;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AbstractClass", mappedBy="proprietaire")
     */
    private $abstracts;

    /**
     * @var \AppBundle\Entity\IncriptionDeGroupe
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\IncriptionDeGroupe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="incription_de_groupe_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $incriptionDeGroupe;

    /**
     * @var \AppBundle\Entity\Inscription
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Inscription")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inscription_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $inscription;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AbstractHasRelecteur", mappedBy="relecteur",cascade={"all"})
     */
    private $abstractHasRelecteurs;

    /**
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true)
     */
    private $facebookId;

    private $facebookAccessToken;
    /**
     * @ORM\Column(name="linkedin_id", type="string", length=255, nullable=true)
     */
    private $linkedinId;

    private $linkedinAccessToken;
    /**
     * @ORM\Column(name="google_id", type="string", length=255, nullable=true)
     */
    private $googleId;

    private $googleAccessToken;

    /**
     * @return mixed
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param mixed $facebookId
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
    }

    /**
     * @return mixed
     */
    public function getFacebookAccessToken()
    {
        return $this->facebookAccessToken;
    }

    /**
     * @param mixed $facebookAccessToken
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebookAccessToken = $facebookAccessToken;
    }

    /**
     * @return mixed
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * @param mixed $googleId
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
    }

    /**
     * @return mixed
     */
    public function getGoogleAccessToken()
    {
        return $this->googleAccessToken;
    }

    /**
     * @param mixed $googleAccessToken
     */
    public function setGoogleAccessToken($googleAccessToken)
    {
        $this->googleAccessToken = $googleAccessToken;
    }

    /**
     * @return mixed
     */
    public function getLinkedinId()
    {
        return $this->linkedinId;
    }

    /**
     * @param mixed $linkedinId
     */
    public function setLinkedinId($linkedinId)
    {
        $this->linkedinId = $linkedinId;
    }

    /**
     * @return mixed
     */
    public function getLinkedinAccessToken()
    {
        return $this->linkedinAccessToken;
    }

    /**
     * @param mixed $linkedinAccessToken
     */
    public function setLinkedinAccessToken($linkedinAccessToken)
    {
        $this->linkedinAccessToken = $linkedinAccessToken;
    }
    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->setRoles(array('ROLE_USER'));
        $this->abstractHasRelecteurs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->telephones = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domaineEtude
     *
     * @param string $domaineEtude
     *
     * @return User
     */
    public function setDomaineEtude($domaineEtude)
    {
        $this->domaineEtude = $domaineEtude;

        return $this;
    }

    /**
     * Get domaineEtude
     *
     * @return string
     */
    public function getDomaineEtude()
    {
        return $this->domaineEtude;
    }

    /**
     * Set niveauAcademique
     *
     * @param string $niveauAcademique
     *
     * @return User
     */
    public function setNiveauAcademique($niveauAcademique)
    {
        $this->niveauAcademique = $niveauAcademique;

        return $this;
    }

    /**
     * Get niveauAcademique
     *
     * @return string
     */
    public function getNiveauAcademique()
    {
        return $this->niveauAcademique;
    }

    /**
     * Add abstract
     *
     * @param \AppBundle\Entity\AbstractClass $abstract
     *
     * @return User
     */
    public function addAbstract(AbstractClass $abstract)
    {
        $this->abstracts[] = $abstract;

        return $this;
    }

    /**
     * Remove abstract
     *
     * @param \AppBundle\Entity\AbstractClass $abstract
     */
    public function removeAbstract(AbstractClass $abstract)
    {
        $this->abstracts->removeElement($abstract);
    }

    /**
     * Get abstracts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAbstracts()
    {
        return $this->abstracts;
    }

    /**
     * Set salutation
     *
     * @param string $salutation
     *
     * @return User
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;

        return $this;
    }

    /**
     * Get salutation
     *
     * @return string
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set jobTitle
     *
     * @param string $jobTitle
     *
     * @return User
     */
    public function setJobTitle($jobTitle)
    {
        $this->jobTitle = $jobTitle;

        return $this;
    }

    /**
     * Get jobTitle
     * @return string
     */
    public function getJobTitle()
    {
        return $this->jobTitle;
    }

    /**
     * Set addressLine
     * @param string $addressLine
     * @return User
     */
    public function setAddressLine($addressLine)
    {
        $this->addressLine = $addressLine;

        return $this;
    }

    /**
     * Get addressLine
     *
     * @return string
     */
    public function getAddressLine()
    {
        return $this->addressLine;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return User
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set website
     *
     * @param string $website
     *
     * @return User
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set incriptionDeGroupe
     *
     * @param \AppBundle\Entity\IncriptionDeGroupe $incriptionDeGroupe
     *
     * @return User
     */
    public function setIncriptionDeGroupe(IncriptionDeGroupe $incriptionDeGroupe = null)
    {
        $this->incriptionDeGroupe = $incriptionDeGroupe;

        return $this;
    }

    /**
     * Get incriptionDeGroupe
     *
     * @return \AppBundle\Entity\IncriptionDeGroupe
     */
    public function getIncriptionDeGroupe()
    {
        return $this->incriptionDeGroupe;
    }

    /**
     * Set inscription
     *
     * @param \AppBundle\Entity\Inscription $inscription
     *
     * @return User
     */
    public function setInscription(Inscription $inscription = null)
    {
        $this->inscription = $inscription;

        return $this;
    }

    /**
     * Get inscription
     *
     * @return \AppBundle\Entity\Inscription
     */
    public function getInscription()
    {
        return $this->inscription;
    }


    /**
     * Add abstractHasRelecteur
     *
     * @param \AppBundle\Entity\AbstractHasRelecteur $abstractHasRelecteur
     *
     * @return User
     */
    public function addAbstractHasRelecteur(\AppBundle\Entity\AbstractHasRelecteur $abstractHasRelecteur)
    {
        $this->abstractHasRelecteurs[] = $abstractHasRelecteur;

        return $this;
    }

    /**
     * Remove abstractHasRelecteur
     *
     * @param \AppBundle\Entity\AbstractHasRelecteur $abstractHasRelecteur
     */
    public function removeAbstractHasRelecteur(\AppBundle\Entity\AbstractHasRelecteur $abstractHasRelecteur)
    {
        $this->abstractHasRelecteurs->removeElement($abstractHasRelecteur);
    }

    /**
     * Get abstractHasRelecteurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAbstractHasRelecteurs()
    {
        return $this->abstractHasRelecteurs;
    }

    /**
     * Set institution
     *
     * @param \AppBundle\Entity\Inscription $institution
     *
     * @return User
     */
    public function setInstitution(\AppBundle\Entity\Inscription $institution = null)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get institution
     *
     * @return \AppBundle\Entity\Inscription
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * Add telephone
     *
     * @param \AppBundle\Entity\Telephone $telephone
     *
     * @return User
     */
    public function addTelephone(\AppBundle\Entity\Telephone $telephone)
    {
        $this->telephones[] = $telephone;

        return $this;
    }

    /**
     * Remove telephone
     *
     * @param \AppBundle\Entity\Telephone $telephone
     */
    public function removeTelephone(\AppBundle\Entity\Telephone $telephone)
    {
        $this->telephones->removeElement($telephone);
    }

    /**
     * Get telephones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTelephones()
    {
        return $this->telephones;
    }

    /**
     * Set localisation
     *
     * @param \AppBundle\Entity\Localisation $localisation
     *
     * @return User
     */
    public function setLocalisation(\AppBundle\Entity\Localisation $localisation = null)
    {
        $this->localisation = $localisation;

        return $this;
    }

    /**
     * Get localisation
     *
     * @return \AppBundle\Entity\Localisation
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }
}
