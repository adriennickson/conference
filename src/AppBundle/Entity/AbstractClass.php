<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Abstract
 *
 * @ORM\Table(name="abstract_class", indexes={@ORM\Index(name="IDX_75355FA1A76ED395", columns={"proprietaire_id"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AbstractRepository")
 */
class AbstractClass
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255, nullable=false)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="justification", type="text", nullable=false)
     */
    private $justification;

    /**
     * @var string
     *
     * @ORM\Column(name="objectif", type="text", nullable=false)
     */
    private $objectif;

    /**
     * @var string
     *
     * @ORM\Column(name="methodologie", type="text", nullable=false)
     */
    private $methodologie;

    /**
     * @var string
     *
     * @ORM\Column(name="resultats", type="text", nullable=false)
     */
    private $resultats;

    /**
     * @var string
     *
     * @ORM\Column(name="conclusion_or_recommandations", type="text", nullable=false)
     */
    private $conclusionOrRecommandations;

    /**
     * @var boolean
     *
     * @ORM\Column(name="est_valide", type="boolean", nullable=true)
     */
    private $estValide;


    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="proprietaire_id", referencedColumnName="id")
     * })
     */
    private $proprietaire;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\MotClef", inversedBy="abstract", cascade={"persist"})
     * @ORM\JoinTable(name="abstract_has_mot_clef",
     *   joinColumns={
     *     @ORM\JoinColumn(name="abstract_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="motClef_id", referencedColumnName="id")
     *   }
     * )
     */
    private $motclef;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\SousTheme", inversedBy="abstract")
     * @ORM\JoinTable(name="abstract_has_soustheme",
     *   joinColumns={
     *     @ORM\JoinColumn(name="abstract_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="SousTheme_id", referencedColumnName="id")
     *   }
     * )
     */
    private $soustheme;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Session", inversedBy="abstract")
     * @ORM\JoinTable(name="session_has_abstract",
     *   joinColumns={
     *     @ORM\JoinColumn(name="abstract_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="Session_id", referencedColumnName="id")
     *   }
     * )
     */
    private $session;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Auteur", mappedBy="abstractClass",cascade={"all"})
     */
    private $auteurs;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AbstractHasRelecteur", mappedBy="abstract",cascade={"all"})
     */
    private $abstractHasRelecteurs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->motclef = new \Doctrine\Common\Collections\ArrayCollection();
        $this->soustheme = new \Doctrine\Common\Collections\ArrayCollection();
        $this->session = new \Doctrine\Common\Collections\ArrayCollection();
        $this->auteurs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->abstractHasRelecteurs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return AbstractClass
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set estValide
     *
     * @param boolean $estValide
     *
     * @return AbstractClass
     */
    public function setEstValide($estValide)
    {
        $this->estValide = $estValide;

        return $this;
    }

    /**
     * Get estValide
     *
     * @return bool
     */
    public function getEstValide()
    {
        return $this->estValide;
    }

    /**
     * Add auteur
     *
     * @param \AppBundle\Entity\Auteur $auteur
     *
     * @return AbstractClass
     */
    public function addAuteur(Auteur $auteur)
    {
        $this->auteurs[] = $auteur;
        $auteur->setAbstractClass($this);
        return $this;
    }

    /**
     * Remove auteur
     *
     * @param \AppBundle\Entity\Auteur $auteur
     */
    public function removeAuteur(Auteur $auteur)
    {
        $this->auteurs->removeElement($auteur);
        $auteur->setAbstractClass(null);
    }

    /**
     * Get auteurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAuteurs()
    {
        return $this->auteurs;
    }

    /**
     * Set proprietaire
     *
     * @param \AppBundle\Entity\User $proprietaire
     *
     * @return AbstractClass
     */
    public function setProprietaire(User $proprietaire = null)
    {
        $this->proprietaire = $proprietaire;

        return $this;
    }

    /**
     * Get proprietaire
     *
     * @return \AppBundle\Entity\User
     */
    public function getProprietaire()
    {
        return $this->proprietaire;
    }

    /**
     * Add motclef
     *
     * @param \AppBundle\Entity\MotClef $motclef
     *
     * @return AbstractClass
     */
    public function addMotclef(MotClef $motclef)
    {
        $this->motclef[] = $motclef;

        return $this;
    }

    /**
     * Remove motclef
     *
     * @param \AppBundle\Entity\MotClef $motclef
     */
    public function removeMotclef(MotClef $motclef)
    {
        $this->motclef->removeElement($motclef);
    }

    /**
     * Get motclef
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMotclef()
    {
        return $this->motclef;
    }

    /**
     * Add soustheme
     *
     * @param \AppBundle\Entity\SousTheme $soustheme
     *
     * @return AbstractClass
     */
    public function addSoustheme(SousTheme $soustheme)
    {
        $this->soustheme[] = $soustheme;

        return $this;
    }

    /**
     * Remove soustheme
     *
     * @param \AppBundle\Entity\SousTheme $soustheme
     */
    public function removeSoustheme(SousTheme $soustheme)
    {
        $this->soustheme->removeElement($soustheme);
    }

    /**
     * Get soustheme
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSoustheme()
    {
        return $this->soustheme;
    }

    /**
     * Add session
     *
     * @param \AppBundle\Entity\Session $session
     *
     * @return AbstractClass
     */
    public function addSession(Session $session)
    {
        $this->session[] = $session;

        return $this;
    }

    /**
     * Remove session
     *
     * @param \AppBundle\Entity\Session $session
     */
    public function removeSession(Session $session)
    {
        $this->session->removeElement($session);
    }

    /**
     * Get session
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Add abstractHasRelecteur
     *
     * @param \AppBundle\Entity\AbstractHasRelecteur $abstractHasRelecteur
     *
     * @return AbstractClass
     */
    public function addAbstractHasRelecteur(\AppBundle\Entity\AbstractHasRelecteur $abstractHasRelecteur)
    {
        $this->abstractHasRelecteurs[] = $abstractHasRelecteur;

        return $this;
    }

    /**
     * Remove abstractHasRelecteur
     *
     * @param \AppBundle\Entity\AbstractHasRelecteur $abstractHasRelecteur
     */
    public function removeAbstractHasRelecteur(\AppBundle\Entity\AbstractHasRelecteur $abstractHasRelecteur)
    {
        $this->abstractHasRelecteurs->removeElement($abstractHasRelecteur);
    }

    /**
     * Get abstractHasRelecteurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAbstractHasRelecteurs()
    {
        return $this->abstractHasRelecteurs;
    }

    /**
     * Set justification
     *
     * @param string $justification
     *
     * @return AbstractClass
     */
    public function setJustification($justification)
    {
        $this->justification = $justification;

        return $this;
    }

    /**
     * Get justification
     *
     * @return string
     */
    public function getJustification()
    {
        return $this->justification;
    }

    /**
     * Set objectif
     *
     * @param string $objectif
     *
     * @return AbstractClass
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif
     *
     * @return string
     */
    public function getObjectif()
    {
        return $this->objectif;
    }

    /**
     * Set methodologie
     *
     * @param string $methodologie
     *
     * @return AbstractClass
     */
    public function setMethodologie($methodologie)
    {
        $this->methodologie = $methodologie;

        return $this;
    }

    /**
     * Get methodologie
     *
     * @return string
     */
    public function getMethodologie()
    {
        return $this->methodologie;
    }

    /**
     * Set resultats
     *
     * @param string $resultats
     *
     * @return AbstractClass
     */
    public function setResultats($resultats)
    {
        $this->resultats = $resultats;

        return $this;
    }

    /**
     * Get resultats
     *
     * @return string
     */
    public function getResultats()
    {
        return $this->resultats;
    }

    /**
     * Set conclusionOrRecommandations
     *
     * @param string $conclusionOrRecommandations
     *
     * @return AbstractClass
     */
    public function setConclusionOrRecommandations($conclusionOrRecommandations)
    {
        $this->conclusionOrRecommandations = $conclusionOrRecommandations;

        return $this;
    }

    /**
     * Get conclusionOrRecommandations
     *
     * @return string
     */
    public function getConclusionOrRecommandations()
    {
        return $this->conclusionOrRecommandations;
    }
}
