<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParametreLabel
 *
 * @ORM\Table(name="parametre_label")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ParametreLabelRepository")
 */
class ParametreLabel
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=25)
     */
    private $ref;

    /**
     * @var string
     *
     * @ORM\Column(name="labelName", type="string", length=255)
     */
    private $labelName;

    /**
     * @var Parametre
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Parametre", cascade={"persist","remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $parametre;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set labelName
     *
     * @param string $labelName
     *
     * @return ParametreLabel
     */
    public function setLabelName($labelName)
    {
        $this->labelName = $labelName;

        return $this;
    }

    /**
     * Get labelName
     *
     * @return string
     */
    public function getLabelName()
    {
        return $this->labelName;
    }

    /**
     * Set parametre
     *
     * @param \AppBundle\Entity\Parametre $parametre
     *
     * @return ParametreLabel
     */
    public function setParametre(\AppBundle\Entity\Parametre $parametre = null)
    {
        $this->parametre = $parametre;

        return $this;
    }

    /**
     * Get parametre
     *
     * @return \AppBundle\Entity\Parametre
     */
    public function getParametre()
    {
        return $this->parametre;
    }

    /**
     * Set ref
     *
     * @param string $ref
     *
     * @return ParametreLabel
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }
}
