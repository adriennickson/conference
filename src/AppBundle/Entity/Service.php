<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 *
 * @ORM\Table(name="service")
 * @ORM\Entity
 */
class Service
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\IncriptionDeGroupe", mappedBy="service")
     */
    private $incriptionDeGroupe;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Inscription", mappedBy="service")
     */
    private $inscription;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->incriptionDeGroupe = new \Doctrine\Common\Collections\ArrayCollection();
        $this->inscription = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add incriptionDeGroupe
     *
     * @param \AppBundle\Entity\IncriptionDeGroupe $incriptionDeGroupe
     *
     * @return Service
     */
    public function addIncriptionDeGroupe(\AppBundle\Entity\IncriptionDeGroupe $incriptionDeGroupe)
    {
        $this->incriptionDeGroupe[] = $incriptionDeGroupe;

        return $this;
    }

    /**
     * Remove incriptionDeGroupe
     *
     * @param \AppBundle\Entity\IncriptionDeGroupe $incriptionDeGroupe
     */
    public function removeIncriptionDeGroupe(\AppBundle\Entity\IncriptionDeGroupe $incriptionDeGroupe)
    {
        $this->incriptionDeGroupe->removeElement($incriptionDeGroupe);
    }

    /**
     * Get incriptionDeGroupe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIncriptionDeGroupe()
    {
        return $this->incriptionDeGroupe;
    }

    /**
     * Add inscription
     *
     * @param \AppBundle\Entity\Inscription $inscription
     *
     * @return Service
     */
    public function addInscription(\AppBundle\Entity\Inscription $inscription)
    {
        $this->inscription[] = $inscription;

        return $this;
    }

    /**
     * Remove inscription
     *
     * @param \AppBundle\Entity\Inscription $inscription
     */
    public function removeInscription(\AppBundle\Entity\Inscription $inscription)
    {
        $this->inscription->removeElement($inscription);
    }

    /**
     * Get inscription
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInscription()
    {
        return $this->inscription;
    }
}
