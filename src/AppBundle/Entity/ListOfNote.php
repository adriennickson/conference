<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

class ListOfNote
{
    protected $notes;
    /**
     * @var string
     */
    protected $observation;
    /**
     * @var string
     */
    protected $comentaire;

    public function __construct()
    {
        $this->notes = new ArrayCollection();
    }

    /**
     * @return ArrayCollection
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param ArrayCollection $notes
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
    }

    public function addNotes($note)
    {
        $this->notes->add($note);
    }

    public function removeNotes($note)
    {
        $this->notes->removeElement($note);
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return string
     */
    public function getComentaire()
    {
        return $this->comentaire;
    }

    /**
     * @param string $comentaire
     */
    public function setComentaire($comentaire)
    {
        $this->comentaire = $comentaire;
    }

}

