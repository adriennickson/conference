<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CritereDeNotation
 *
 * @ORM\Table(name="critere_de_notation")
 * @ORM\Entity
 */
class CritereDeNotation
{
    /**
     * @var string
     *
     * @ORM\Column(name="critere", type="string", length=45, nullable=false)
     */
    private $critere;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set critere
     *
     * @param string $critere
     *
     * @return CritereDeNotation
     */
    public function setCritere($critere)
    {
        $this->critere = $critere;

        return $this;
    }

    /**
     * Get critere
     *
     * @return string
     */
    public function getCritere()
    {
        return $this->critere;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
