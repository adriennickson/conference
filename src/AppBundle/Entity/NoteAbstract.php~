<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NoteAbstract
 *
 * @ORM\Table(name="note_abstract", indexes={@ORM\Index(name="fk_note_abstract_critere_de_notation1_idx", columns={"critere_de_notation_id"}), @ORM\Index(name="fk_abstract_has_relecteur_note_abstract1_idx", columns={"abstract_id"})})
 * @ORM\Entity
 */
class NoteAbstract
{
    /**
     * @var integer
     *
     * @ORM\Column(name="note", type="integer", nullable=true)
     */
    private $note;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\CritereDeNotation
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CritereDeNotation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="critere_de_notation_id", referencedColumnName="id")
     * })
     */
    private $critereDeNotation;

    /**
     * @var \AppBundle\Entity\AbstractHasRelecteur
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AbstractHasRelecteur", inversedBy="noteAbstracts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="abstract_id", referencedColumnName="id")
     * })
     */
    private $abstractHasRelecteur;


    /**
     * Set note
     *
     * @param integer $note
     *
     * @return NoteAbstract
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return integer
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return NoteAbstract
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set critereDeNotation
     *
     * @param \AppBundle\Entity\CritereDeNotation $critereDeNotation
     *
     * @return NoteAbstract
     */
    public function setCritereDeNotation(\AppBundle\Entity\CritereDeNotation $critereDeNotation)
    {
        $this->critereDeNotation = $critereDeNotation;

        return $this;
    }

    /**
     * Get critereDeNotation
     *
     * @return \AppBundle\Entity\CritereDeNotation
     */
    public function getCritereDeNotation()
    {
        return $this->critereDeNotation;
    }


    /**
     * Set abstractHasRelecteur
     *
     * @param \AppBundle\Entity\AbstractHasRelecteur $abstractHasRelecteur
     *
     * @return NoteAbstract
     */
    public function setAbstractHasRelecteur(\AppBundle\Entity\AbstractHasRelecteur $abstractHasRelecteur = null)
    {
        $this->abstractHasRelecteur = $abstractHasRelecteur;

        return $this;
    }

    /**
     * Get abstractHasRelecteur
     *
     * @return \AppBundle\Entity\AbstractHasRelecteur
     */
    public function getAbstractHasRelecteur()
    {
        return $this->abstractHasRelecteur;
    }
}
