<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IncriptionDeGroupe
 *
 * @ORM\Table(name="incription_de_groupe", indexes={@ORM\Index(name="fk_incription_de_groupe_payement1_idx", columns={"payement_id"})})
 * @ORM\Entity
 */
class IncriptionDeGroupe
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Payement
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Payement")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payement_id", referencedColumnName="id")
     * })
     */
    private $payement;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Service", inversedBy="incriptionDeGroupe")
     * @ORM\JoinTable(name="incription_de_groupe_has_service",
     *   joinColumns={
     *     @ORM\JoinColumn(name="incription_de_groupe_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     *   }
     * )
     */
    private $service;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->service = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set payement
     *
     * @param \AppBundle\Entity\Payement $payement
     *
     * @return IncriptionDeGroupe
     */
    public function setPayement(\AppBundle\Entity\Payement $payement = null)
    {
        $this->payement = $payement;

        return $this;
    }

    /**
     * Get payement
     *
     * @return \AppBundle\Entity\Payement
     */
    public function getPayement()
    {
        return $this->payement;
    }

    /**
     * Add service
     *
     * @param \AppBundle\Entity\Service $service
     *
     * @return IncriptionDeGroupe
     */
    public function addService(\AppBundle\Entity\Service $service)
    {
        $this->service[] = $service;

        return $this;
    }

    /**
     * Remove service
     *
     * @param \AppBundle\Entity\Service $service
     */
    public function removeService(\AppBundle\Entity\Service $service)
    {
        $this->service->removeElement($service);
    }

    /**
     * Get service
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getService()
    {
        return $this->service;
    }
}
