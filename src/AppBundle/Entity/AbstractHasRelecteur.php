<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AbstractHasRelecteur
 *
 * @ORM\Table(name="abstract_has_relecteur", indexes={@ORM\Index(name="fk_abstract_has_user_user1_idx", columns={"relecteur_id"}), @ORM\Index(name="fk_abstract_has_user_abstract1_idx", columns={"abstract_id"})})
 * @ORM\Entity
 */
class AbstractHasRelecteur
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $observation;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $commentaire;

    /**
     * @var \AppBundle\Entity\NoteAbstract
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\NoteAbstract", cascade={"remove"}, mappedBy="abstractHasRelecteur")
     */
    private $noteAbstracts;

    /**
     * @var \AppBundle\Entity\AbstractClass
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AbstractClass", inversedBy="abstractHasRelecteurs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="abstract_id", referencedColumnName="id")
     * })
     */
    private $abstract;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="relecteur_id", referencedColumnName="id")
     * })
     */
    private $relecteur;

    /**
     * Set abstract
     *
     * @param \AppBundle\Entity\AbstractClass $abstract
     *
     * @return AbstractHasRelecteur
     */
    public function setAbstract(AbstractClass $abstract)
    {
        $this->abstract = $abstract;

        return $this;
    }

    /**
     * Get abstract
     *
     * @return \AppBundle\Entity\AbstractClass
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * Set relecteur
     *
     * @param \AppBundle\Entity\User $relecteur
     *
     * @return AbstractHasRelecteur
     */
    public function setRelecteur(User $relecteur)
    {
        $this->relecteur = $relecteur;

        return $this;
    }

    /**
     * Get relecteur
     *
     * @return \AppBundle\Entity\User
     */
    public function getRelecteur()
    {
        return $this->relecteur;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->noteAbstracts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add noteAbstract
     *
     * @param \AppBundle\Entity\NoteAbstract $noteAbstract
     *
     * @return AbstractHasRelecteur
     */
    public function addNoteAbstract(\AppBundle\Entity\NoteAbstract $noteAbstract)
    {
        $this->noteAbstracts[] = $noteAbstract;

        return $this;
    }

    /**
     * Remove noteAbstract
     *
     * @param \AppBundle\Entity\NoteAbstract $noteAbstract
     */
    public function removeNoteAbstract(\AppBundle\Entity\NoteAbstract $noteAbstract)
    {
        $this->noteAbstracts->removeElement($noteAbstract);
    }

    /**
     * Get noteAbstracts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteAbstracts()
    {
        return $this->noteAbstracts;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return AbstractHasRelecteur
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set observation
     *
     * @param string $observation
     *
     * @return AbstractHasRelecteur
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     *
     * @return AbstractHasRelecteur
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }
}
