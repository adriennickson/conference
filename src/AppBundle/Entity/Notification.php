<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table(name="notification", indexes={@ORM\Index(name="fk_notification_user1_idx", columns={"user_id"})})
 * @ORM\Entity
 */
class Notification
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="envoie", type="boolean", nullable=false)
     */
    private $envoie;

    /**
     * @var boolean
     *
     * @ORM\Column(name="lue", type="boolean", nullable=false)
     */
    private $lue;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=45, nullable=true)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="contennu", type="string", length=45, nullable=true)
     */
    private $contennu;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Set envoie
     *
     * @param boolean $envoie
     *
     * @return Notification
     */
    public function setEnvoie($envoie)
    {
        $this->envoie = $envoie;

        return $this;
    }

    /**
     * Get envoie
     *
     * @return boolean
     */
    public function getEnvoie()
    {
        return $this->envoie;
    }

    /**
     * Set lue
     *
     * @param boolean $lue
     *
     * @return Notification
     */
    public function setLue($lue)
    {
        $this->lue = $lue;

        return $this;
    }

    /**
     * Get lue
     *
     * @return boolean
     */
    public function getLue()
    {
        return $this->lue;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Notification
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set contennu
     *
     * @param string $contennu
     *
     * @return Notification
     */
    public function setContennu($contennu)
    {
        $this->contennu = $contennu;

        return $this;
    }

    /**
     * Get contennu
     *
     * @return string
     */
    public function getContennu()
    {
        return $this->contennu;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Notification
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Notification
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
