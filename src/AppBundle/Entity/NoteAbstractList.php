<?php
/**
 * Created by PhpStorm.
 * User: Mr MOMO
 * Date: 13/09/2017
 * Time: 16:12
 */

namespace AppBundle\Entity;


class NoteAbstractList
{
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteAbstracts;

    /**
     * NoteAbstractList constructor.
     */
    public function __construct()
    {
        $this->noteAbstracts = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteAbstracts()
    {
        return $this->noteAbstracts;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $noteAbstracts
     */
    public function setNoteAbstracts($noteAbstracts)
    {
        $this->noteAbstracts = $noteAbstracts;
    }

}