<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="session", indexes={@ORM\Index(name="fk_Session_CreneauHoraire1_idx", columns={"CreneauHoraire_id"})})
 * @ORM\Entity
 */
class Session
{
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45, nullable=false)
     */
    private $type;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\CreneauHoraire
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CreneauHoraire")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CreneauHoraire_id", referencedColumnName="id")
     * })
     */
    private $creneauhoraire;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\AbstractClass", mappedBy="session")
     */
    private $abstract;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\SousTheme", inversedBy="session")
     * @ORM\JoinTable(name="session_has_sous_theme",
     *   joinColumns={
     *     @ORM\JoinColumn(name="Session_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="SousTheme_id", referencedColumnName="id")
     *   }
     * )
     */
    private $soustheme;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->abstract = new \Doctrine\Common\Collections\ArrayCollection();
        $this->soustheme = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set type
     *
     * @param string $type
     *
     * @return Session
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creneauhoraire
     *
     * @param \AppBundle\Entity\CreneauHoraire $creneauhoraire
     *
     * @return Session
     */
    public function setCreneauhoraire(\AppBundle\Entity\CreneauHoraire $creneauhoraire = null)
    {
        $this->creneauhoraire = $creneauhoraire;

        return $this;
    }

    /**
     * Get creneauhoraire
     *
     * @return \AppBundle\Entity\CreneauHoraire
     */
    public function getCreneauhoraire()
    {
        return $this->creneauhoraire;
    }

    /**
     * Add abstract
     *
     * @param \AppBundle\Entity\AbstractClass $abstract
     *
     * @return Session
     */
    public function addAbstract(\AppBundle\Entity\AbstractClass $abstract)
    {
        $this->abstract[] = $abstract;

        return $this;
    }

    /**
     * Remove abstract
     *
     * @param \AppBundle\Entity\AbstractClass $abstract
     */
    public function removeAbstract(\AppBundle\Entity\AbstractClass $abstract)
    {
        $this->abstract->removeElement($abstract);
    }

    /**
     * Get abstract
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * Add soustheme
     *
     * @param \AppBundle\Entity\SousTheme $soustheme
     *
     * @return Session
     */
    public function addSoustheme(\AppBundle\Entity\SousTheme $soustheme)
    {
        $this->soustheme[] = $soustheme;

        return $this;
    }

    /**
     * Remove soustheme
     *
     * @param \AppBundle\Entity\SousTheme $soustheme
     */
    public function removeSoustheme(\AppBundle\Entity\SousTheme $soustheme)
    {
        $this->soustheme->removeElement($soustheme);
    }

    /**
     * Get soustheme
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSoustheme()
    {
        return $this->soustheme;
    }
}
