<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MotClef
 *
 * @ORM\Table(name="mot_clef")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MotClefRepository")
 */
class MotClef
{
    /**
     * @var string
     *
     * @ORM\Column(name="mot", type="string", length=45, nullable=false)
     */
    private $mot;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\AbstractClass", mappedBy="motclef")
     */
    private $abstract;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->abstract = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set mot
     *
     * @param string $mot
     *
     * @return MotClef
     */
    public function setMot($mot)
    {
        $this->mot = $mot;

        return $this;
    }

    /**
     * Get mot
     *
     * @return string
     */
    public function getMot()
    {
        return $this->mot;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add abstract
     *
     * @param \AppBundle\Entity\AbstractClass $abstract
     *
     * @return MotClef
     */
    public function addAbstract(\AppBundle\Entity\AbstractClass $abstract)
    {
        $this->abstract[] = $abstract;

        return $this;
    }

    /**
     * Remove abstract
     *
     * @param \AppBundle\Entity\AbstractClass $abstract
     */
    public function removeAbstract(\AppBundle\Entity\AbstractClass $abstract)
    {
        $this->abstract->removeElement($abstract);
    }

    /**
     * Get abstract
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAbstract()
    {
        return $this->abstract;
    }
}
