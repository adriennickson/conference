<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Image;
use Doctrine\ORM\Mapping as ORM;

/**
 * Speaker
 *
 * @ORM\Table(name="speaker")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SpeakerRepository")
 */
class Speaker
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nomComplet", type="string", length=255, unique=true)
     */
    private $nomComplet;

    /**
     * @var string
     *
     * @ORM\Column(name="biographie", type="string", length=255)
     */
    private $biographie;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $adresseFaceBook;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $adresseTweeter;

    /**
     * @var Image
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Image", cascade={"persist","remove"})
     */
    private $photo;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomComplet
     *
     * @param string $nomComplet
     *
     * @return Speaker
     */
    public function setNomComplet($nomComplet)
    {
        $this->nomComplet = $nomComplet;

        return $this;
    }

    /**
     * Get nomComplet
     *
     * @return string
     */
    public function getNomComplet()
    {
        return $this->nomComplet;
    }

    /**
     * Set photo
     *
     * @param \AppBundle\Entity\Image $photo
     *
     * @return Speaker
     */
    public function setPhoto(\AppBundle\Entity\Image $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return \AppBundle\Entity\Image
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set adresseFaceBook
     *
     * @param string $adresseFaceBook
     *
     * @return Speaker
     */
    public function setAdresseFaceBook($adresseFaceBook)
    {
        $this->adresseFaceBook = $adresseFaceBook;

        return $this;
    }

    /**
     * Get adresseFaceBook
     *
     * @return string
     */
    public function getAdresseFaceBook()
    {
        return $this->adresseFaceBook;
    }

    /**
     * Set adresseTweeter
     *
     * @param string $adresseTweeter
     *
     * @return Speaker
     */
    public function setAdresseTweeter($adresseTweeter)
    {
        $this->adresseTweeter = $adresseTweeter;

        return $this;
    }

    /**
     * Get adresseTweeter
     *
     * @return string
     */
    public function getAdresseTweeter()
    {
        return $this->adresseTweeter;
    }

    /**
     * Set biographie
     *
     * @param string $biographie
     *
     * @return Speaker
     */
    public function setBiographie($biographie)
    {
        $this->biographie = $biographie;

        return $this;
    }

    /**
     * Get biographie
     *
     * @return string
     */
    public function getBiographie()
    {
        return $this->biographie;
    }
}
