<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Auteur
 *
 * @ORM\Table(name="auteur")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AuteurRepository")
 */
class Auteur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $nom;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $prenom;

    /**
     * @var Collection
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Email",
     *     mappedBy="auteur",
     *     orphanRemoval=true,
     *     cascade={"persist","remove"}
     * )
     */
    private $emails;

    /**
     * @var Collection
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Telephone",
     *     mappedBy="auteur",
     *     orphanRemoval=true,
     *     cascade={"persist","remove"}
     * )
     */
    private $telephones;

    /**
     * @var Specialite
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Specialite", cascade={"persist","remove"})
     */
    private $specialite;

    /**
     * @var Institution
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Institution", cascade={"persist","remove"})
     */
    private $institution;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $niveauAcademique;

    /**
     * sellect entre simple coauteur, presentateur et correspondant
     * @var string
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AbstractClass", inversedBy="auteurs",cascade={"all"})
     * @ORM\JoinColumn(name="abstract_id", referencedColumnName="id")
     */
    private $abstractClass;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Auteur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set niveauAcademique
     *
     * @param string $niveauAcademique
     *
     * @return Auteur
     */
    public function setNiveauAcademique($niveauAcademique)
    {
        $this->niveauAcademique = $niveauAcademique;

        return $this;
    }

    /**
     * Get niveauAcademique
     *
     * @return string
     */
    public function getNiveauAcademique()
    {
        return $this->niveauAcademique;
    }

    /**
     * Set abstractClass
     *
     * @param \AppBundle\Entity\AbstractClass $abstractClass
     *
     * @return Auteur
     */
    public function setAbstractClass(\AppBundle\Entity\AbstractClass $abstractClass = null)
    {
        $this->abstractClass = $abstractClass;

        return $this;
    }

    /**
     * Get abstractClass
     *
     * @return \AppBundle\Entity\AbstractClass
     */
    public function getAbstractClass()
    {
        return $this->abstractClass;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->emails = new \Doctrine\Common\Collections\ArrayCollection();
        $this->telephones = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Auteur
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Auteur
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Add email
     *
     * @param \AppBundle\Entity\Email $email
     *
     * @return Auteur
     */
    public function addEmail(\AppBundle\Entity\Email $email)
    {
        $this->emails[] = $email;

        return $this;
    }

    /**
     * Remove email
     *
     * @param \AppBundle\Entity\Email $email
     */
    public function removeEmail(\AppBundle\Entity\Email $email)
    {
        $this->emails->removeElement($email);
    }

    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Add telephone
     *
     * @param \AppBundle\Entity\Telephone $telephone
     *
     * @return Auteur
     */
    public function addTelephone(\AppBundle\Entity\Telephone $telephone)
    {
        $this->telephones[] = $telephone;

        return $this;
    }

    /**
     * Remove telephone
     *
     * @param \AppBundle\Entity\Telephone $telephone
     */
    public function removeTelephone(\AppBundle\Entity\Telephone $telephone)
    {
        $this->telephones->removeElement($telephone);
    }

    /**
     * Get telephones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTelephones()
    {
        return $this->telephones;
    }

    /**
     * Set specialite
     *
     * @param \AppBundle\Entity\Specialite $specialite
     *
     * @return Auteur
     */
    public function setSpecialite(\AppBundle\Entity\Specialite $specialite = null)
    {
        $this->specialite = $specialite;

        return $this;
    }

    /**
     * Get specialite
     *
     * @return \AppBundle\Entity\Specialite
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    /**
     * Set institution
     *
     * @param \AppBundle\Entity\Institution $institution
     *
     * @return Auteur
     */
    public function setInstitution(\AppBundle\Entity\Institution $institution = null)
    {
        $this->institution = $institution;

        return $this;
    }

    /**
     * Get institution
     *
     * @return \AppBundle\Entity\Institution
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    public function __toString()
    {
        return strval( $this->getId() );
    }
}
