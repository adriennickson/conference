<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        $dateDeLaConference = $this->getDoctrine()->getRepository('AppBundle:Parametre')->findOneBy(array("clef"=>"dateConf"));
        if($dateDeLaConference!=null)
            $dayLeft = intval(substr($dateDeLaConference->getValeur(),1,1));
        else
            $dayLeft = null;

        return $this->render(
            'default/index.html.twig',
            array(
                'dayJeft' => $dayLeft
            )
        );
    }
}
