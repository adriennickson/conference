<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AuteurType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, array(
                'attr'=>array('class'=>'form-control')
            ))
            ->add('prenom', TextType::class, array(
                'attr'=>array('class'=>'form-control')
            ))
            ->add('emails',CollectionType::class,array(
                'entry_type' => EmailType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false,
                'attr'=>array('class'=>'well')
            ))
            ->add('telephones',CollectionType::class,array(
                'entry_type' => TelephoneType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false,
                'attr'=>array('class'=>'well')
            ))
            ->add('specialite', SpecialiteType::class, array(
                'attr'=>array('class'=>'well')
            ))
            ->add('niveauAcademique',ChoiceType::class,array(
                'choices'=>array(
                    'Pr' => 'Pr',
                    'MC' => 'MC',
                    'Phd' => 'Phd',
                    'Masters' => 'Masters',
                    'Licence' => 'Licence',
                    'BAC' => 'BAC'
                ), 'attr'=>array('class'=>'form-control')
            ))
            ->add('institution', InstitutionType::class, array(
                'attr'=>array('class'=>'well')
            ))
            ->add('type',ChoiceType::class,array(
                'choices'=>array(
                    'coauteur' => 'simple coauteur',
                    'presentateur' => 'presentateur',
                    'correspondant' => 'correspondant'
                ), 'attr'=>array('class'=>'form-control')
            ))
        ;
//            ->add('abstractClass');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Auteur'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_auteur';
    }


}
