<?php

namespace AppBundle\Form;

use AppBundle\Entity\NoteAbstract;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListOfNoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('notes', CollectionType::class,array(
                'entry_type' => NoteAbstractType::class
            ))
            ->add('observation',ChoiceType::class,array(
                'choices' => array(
                    'valider en presentation' => 'valider en presentation',
                    'valider en poster' => 'valider en poster',
                    'accepter avec amelioration' => 'accepter avec amelioration',
                    'rejeter' => 'rejeter'
                )
            ))
            ->add('comentaire', TextareaType::class)
            ->add('Noter', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ListOfNote'
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_list_of_note_type';
    }
}
