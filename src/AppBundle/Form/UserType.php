<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('salutation')
            ->add('username',TextType::class, array('required'=>true))
            ->add('lastName')
            ->add('email')
            ->add('jobTitle')
            ->add('addressLine')
            ->add('postalCode')
            ->add('website')
            ->add('domaineEtude')
            ->add('niveauAcademique')
            ->add('institution')
            ->add('localisation', LocalisationType::class)
            ->add('incriptionDeGroupe')
            ->add('inscription')
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
