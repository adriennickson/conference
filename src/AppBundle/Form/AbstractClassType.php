<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AbstractClassType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre',TextType::class,array(
                'required' => true
            ))
            ->add('justification',TextType::class,array(
                'required' => false
            ))
            ->add('objectif',TextType::class,array(
                'required' => false
            ))
            ->add('methodologie',TextType::class,array(
                'required' => false
            ))
            ->add('resultats',TextType::class,array(
                'required' => false
            ))
            ->add('conclusionOrRecommandations',TextType::class,array(
                'required' => false
            ))
            ->add('auteurs',CollectionType::class,array(
                'entry_type' => AuteurType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false,
                'label_attr' => array(
//                    'href' => '#appbundle_abstractclass_auteurs',
//                    'data-toggle' => 'collapse'
                ),
                'attr'=>array(
                    'class'=>'well',
                )
            ))
            ->add('motclef',CollectionType::class,array(
                'entry_type' => MotClefType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'by_reference' => false
            ))
            ->add('soumettre', SubmitType::class, array(
                'attr' => array(
                    'class' => 'btn btn-primary'
                )
            ))

        ;
//            ->add('estValide');
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\AbstractClass'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_abstractclass';
    }


}
