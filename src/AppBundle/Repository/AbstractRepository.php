<?php

namespace AppBundle\Repository;

/**
 * AbstractRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AbstractRepository extends \Doctrine\ORM\EntityRepository
{
    public function findByRelecteur(\AppBundle\Entity\User $relecteur){
        return
            $this->createQueryBuilder('a')
                ->join('a.abstractHasRelecteurs','ahr')
                ->join('ahr.relecteur', 'r')
                ->where('r.id = :id')
                ->setParameter('id',$relecteur->getId())
                ->getQuery()
                ->getResult();
        ;
    }
}
