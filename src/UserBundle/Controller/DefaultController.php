<?php

namespace UserBundle\Controller;

use AppBundle\Entity\AbstractClass;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $abstractClass = new AbstractClass();
        $form = $this->createForm('AppBundle\Form\AbstractClassType', $abstractClass);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $abstractClasses = $em->getRepository('AppBundle:AbstractClass')->findBy(array(
            'proprietaire'=>$this->getUser()
        ));

        if (
            $form->isSubmitted() &&
            $form->isValid() &&
            $abstractClass->getJustification() != null &&
            $abstractClass->getObjectif() != null &&
            $abstractClass->getMethodologie() != null &&
            $abstractClass->getResultats() != null &&
            $abstractClass->getConclusionOrRecommandations() != null
        )
        {
            $abstractClass->setProprietaire($this->getUser());
            foreach ($abstractClass->getAuteurs() as $auteur)
                $auteur->setAbstractClass($abstractClass);
            $em->persist($abstractClass);
            $em->flush();

            return $this->redirectToRoute('homepage', array('id' => $abstractClass->getId()));
        }elseif (
            $form->isSubmitted() &&
            (
                $abstractClass->getJustification() == null ||
                $abstractClass->getObjectif() == null ||
                $abstractClass->getMethodologie() == null ||
                $abstractClass->getResultats() == null ||
                $abstractClass->getConclusionOrRecommandations() == null
            )
        ){
            $this->addFlash('error',"Veuillez bien entrer les 5 champs de l'abstract");
            $urlT = $this->generateUrl('homepage')."#id_soumission";
            $this->redirect($urlT);
        }

        return $this->render('UserBundle:Default:index.html.twig', array(
            'abstractClass' => $abstractClass,
            'abstract_form' => $form->createView(),
            'abstractClasses' => $abstractClasses,
        ));
    }
}