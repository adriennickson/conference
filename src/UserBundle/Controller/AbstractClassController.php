<?php

namespace SoumissionsBundle\Controller;

use SoumissionsBundle\Entity\AbstractClass;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Abstractclass controller.
 *
 */
class AbstractClassController extends Controller
{
    /**
     * Lists all abstractClass entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $abstractClasses = $em->getRepository('SoumissionsBundle:AbstractClass')->findAll();

        return $this->render('@Soumissions/abstractclass/index.html.twig', array(
            'abstractClasses' => $abstractClasses,
        ));
    }

    /**
     * Creates a new abstractClass entity.
     *
     */
    public function newAction(Request $request)
    {
        $abstractClass = new Abstractclass();
        $form = $this->createForm('SoumissionsBundle\Form\AbstractClassType', $abstractClass);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $abstractClass->setUser($this->getUser());
            foreach ($abstractClass->getAuteurs() as $auteur)
                $auteur->setAbstractClass($abstractClass);
            $em->persist($abstractClass);
            $em->flush();

            return $this->redirectToRoute('abstract_show', array('id' => $abstractClass->getId()));
        }

        return $this->render('@Soumissions/abstractclass/new.html.twig', array(
            'abstractClass' => $abstractClass,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a abstractClass entity.
     *
     */
    public function showAction(AbstractClass $abstractClass)
    {
        $deleteForm = $this->createDeleteForm($abstractClass);

        return $this->render('@Soumissions/abstractclass/show.html.twig', array(
            'abstractClass' => $abstractClass,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing abstractClass entity.
     *
     */
    public function editAction(Request $request, AbstractClass $abstractClass)
    {
        $deleteForm = $this->createDeleteForm($abstractClass);
        $editForm = $this->createForm('SoumissionsBundle\Form\AbstractClassType', $abstractClass);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            foreach ($abstractClass->getAuteurs() as $auteur)
                $auteur->setAbstractClass($abstractClass);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('abstract_edit', array('id' => $abstractClass->getId()));
        }

        return $this->render('@Soumissions/abstractclass/edit.html.twig', array(
            'abstractClass' => $abstractClass,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a abstractClass entity.
     *
     */
    public function deleteAction(Request $request, AbstractClass $abstractClass)
    {
        $form = $this->createDeleteForm($abstractClass);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($abstractClass);
            $em->flush();
        }

        return $this->redirectToRoute('abstract_index');
    }

    /**
     * Creates a form to delete a abstractClass entity.
     *
     * @param AbstractClass $abstractClass The abstractClass entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AbstractClass $abstractClass)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('abstract_delete', array('id' => $abstractClass->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
