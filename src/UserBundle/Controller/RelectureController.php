<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class RelectureController extends Controller
{
    /**
     * @Route("/allAbstract")
     */
    public function allAbstractAction()
    {

        return $this->render('UserBundle:Relecture:all_abstract.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/oneAbstract")
     */
    public function oneAbstractAction()
    {
        return $this->render('UserBundle:Relecture:one_abstract.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/noterAbstract")
     */
    public function noterAbstractAction()
    {
        return $this->render('UserBundle:Relecture:noter_abstract.html.twig', array(
            // ...
        ));
    }

}
