<?php

namespace UserBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RelectureControllerTest extends WebTestCase
{
    public function testAllabstract()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/allAbstract');
    }

    public function testOneabstract()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/oneAbstract');
    }

    public function testNoterabstract()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/noterAbstract');
    }

}
