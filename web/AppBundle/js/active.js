jQuery(document).ready(function ($) {



//for hero area 100% innerWidth

    var windowWidth = $(window).width();

    if (windowWidth > 480) {

        $(window).bind('resizeEnd', function () {
            $('.home_content').css('height', $(window).height());
            var windowWidth = $(window).width();
            if (windowWidth < 768) {
                $('.home_content').css('height', 'auto');
            }
        });

        $(window).resize(function () {

            if (this.resizeTo) clearTimeout(this.resizeTo);
            setTimeout(function () {
                $(this).trigger('resizeEnd');
            });

        }).trigger('resize');

    }


//for price ticket padding

    var windowWidth = $(window).width();

    if (windowWidth > 991) {

        var signleTicket = $('.single_price').height() * 3 + 40;
        var ticketHeight = $('.price_left').height();
        var paddingTicket = (signleTicket - ticketHeight) / 2;

        $('.price_left').css({
            paddingTop: paddingTicket + 'px',
            paddingBottom: paddingTicket + 'px'
        });

    }


        var signleTicket = $('.feature_cell > .container').height();
        var ticketHeight = $('.feature_right').height();
        var paddingTicket = (signleTicket - ticketHeight) / 2;

        $('.feature_right').css({
            paddingTop: paddingTicket + 'px',
            paddingBottom: paddingTicket + 'px'
        });

    if (windowWidth < 768 ) {
    
        var signleTicket = $('.f_bg_right').height();
        var ticketHeight = $('.feature_right').height();
        var paddingTicket = (signleTicket - ticketHeight) / 2;

        $('.feature_right').css({
            paddingTop: paddingTicket + 'px',
            paddingBottom: paddingTicket + 'px'
        });
        
        
    }
    
    
    
    
    //for speaker slider

    $('.speaker_slider').owlCarousel({
        items: 4,
        margin: 30,
        nav: true,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        loop: true,
        responsive: {
            0: {
                items: 1,
                margin: 0
            },
            768: {
                items: 3,
                margin: 15
            },
            991: {
                items: 4
            }
        }

    });


//for pratner slider

    $('.partner_slider').owlCarousel({
        items: 6,
        margin: 50,
        nav: true,
        navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        loop: true,
        responsive: {
            0: {
                items: 2,
                margin: 10
            },
            480: {
                items: 3,
                margin: 15
            },
            768: {
                items: 4
            },
            992: {
                items: 5,
                margin: 30
            },
            1201: {
                items: 6
            }
        }
    });


//countdown timer

    $('.count_down').countdown('2017/01/01', function (event) {
        $(this).html(event.strftime('<div class="single_count"><h1>%D</h1><p>Days</p></div><div class="single_count"><h1>%H</h1><p>Hour</p></div><div class="single_count"><h1>%M</h1><p>Minute</p></div><div class="single_count"><h1>%S</h1><p>Secound</p></div>'));
    });


//counter up
    
    $('.counter').counterUp({
        delay: 10,
        time: 1000
    });


//one page nav js

    $('#nav').onePageNav({
        currentClass: 'current',
        changeHash: false,
        scrollSpeed: 750,
        scrollThreshold: 0.5,
        filter: '',
        easing: 'swing',
        begin: function () {
            //I get fired when the animation is starting
        },
        end: function () {
            //I get fired when the animation is ending
        },
        scrollChange: function ($currentListItem) {
            //I get fired when you enter a section and I pass the list item of the section
        }
    });


//mobile menu click func
    
    $('#nav li a').click(function () {
        $('.collapse').removeClass('in');
    });

 //this code is for preloader
    $("body").addClass("preloader_active");


});

$(window).load(function() {
// makes sure the whole site is loaded
    $('#preloader').fadeOut(); // will first fade out the loading animation
    $('.preloader_spinner').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.
    $("body").removeClass("preloader_active");
})

//stick div

$(window).on('scroll', function () {
    if ($(window).scrollTop() > 10) {
        $('.stick').addClass('sticky');
    } else {
        $('.stick').removeClass('sticky');
    }

});